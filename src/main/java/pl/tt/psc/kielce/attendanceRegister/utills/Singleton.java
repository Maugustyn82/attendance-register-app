package pl.tt.psc.kielce.attendanceRegister.utills;

import pl.tt.psc.kielce.attendanceRegister.model.Attendance;
import pl.tt.psc.kielce.attendanceRegister.model.Participant;
import java.util.Set;
import static pl.tt.psc.kielce.attendanceRegister.utills.CSV.getAttendanceData;
import static pl.tt.psc.kielce.attendanceRegister.utills.CSV.getParticipantData;

public class Singleton {

    private static Singleton instance;

   private Set<Participant> baseParticipantData;
   private Set<Attendance> baseAttendanceData;

    private Singleton() {
        this.baseParticipantData = getParticipantData();
        this.baseAttendanceData = getAttendanceData();
    }

    public static Singleton getInstance() {
        if (instance == null) {
            instance = new Singleton();
        }
        return instance;
    }

    public Set<Participant> getBaseParticipantData() {
        return baseParticipantData;
    }

    public Set<Attendance> getBaseAttendanceData() {
        return baseAttendanceData;
    }
}