package pl.tt.psc.kielce.attendanceRegister.model;


import java.util.Objects;

public class Participant implements Comparable<Participant> {

    private String name;
    private String surname;
    private String dateOfBirth;
    private String telephoneNumber;
    private static int staticID;
    private  int participantID;
    private String status;
    private String login;
    private String password;

    public Participant(String name, String surname, String dateOfBirth, String telephoneNumber,
                        String status, String login, String password) {
        this.name = name;
        this.surname = surname;
        this.dateOfBirth = dateOfBirth;
        this.telephoneNumber = telephoneNumber;
        this.staticID++;
        this.participantID = staticID;
        this.status = status;
        this.login = login;
        this.password = password;
    }

    public int getParticipantID() {
        return participantID;
    }

    public String getStatus() {
        return status;
    }

    public String getLogin() {
        return login;
    }

    public String getPassword() {
        return password;
    }

    public String getSurname() {
        return surname;
    }

    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append("Participant:" + "\n");
        sb.append("name = " + name + "|");
        sb.append("surname = " + surname+ "|");
        sb.append("date of birth = " + dateOfBirth+ "|");
        sb.append("telephone number = " + telephoneNumber+ "|");
        sb.append("participantID = " + participantID + "|");
        sb.append("status = " + status+"\n");
        return sb.toString();
    }

    public String toStringCSV() {
        StringBuilder sb = new StringBuilder();
        sb.append(name + ",");
        sb.append(surname + ",");
        sb.append(dateOfBirth + ",");
        sb.append(telephoneNumber + ",");
        sb.append(status + ",");
        sb.append(login + ",");
        sb.append(password);
        return sb.toString();
    }

    @Override
    public int compareTo(Participant o) {
        return this.getSurname().compareToIgnoreCase(o.getSurname());
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof Participant)) return false;
        Participant that = (Participant) o;
        return Objects.equals(name, that.name) &&
                Objects.equals(getSurname(), that.getSurname()) &&
                Objects.equals(dateOfBirth, that.dateOfBirth) &&
                Objects.equals(telephoneNumber, that.telephoneNumber) &&
                Objects.equals(getStatus(), that.getStatus());
    }

    @Override
    public int hashCode() {

        return Objects.hash(name, getSurname(), dateOfBirth, telephoneNumber, getStatus());
    }
}