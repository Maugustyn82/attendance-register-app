package pl.tt.psc.kielce.attendanceRegister;

import pl.tt.psc.kielce.attendanceRegister.utills.Controller;

public class Run {
    public static void main(String[] args) {

        Controller controller = new Controller();

        while (true) {
            controller.execute();
        }
    }
}