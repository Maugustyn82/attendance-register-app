package pl.tt.psc.kielce.attendanceRegister.utills;

import java.util.Scanner;
import static pl.tt.psc.kielce.attendanceRegister.utills.Informations.*;import static pl.tt.psc.kielce.attendanceRegister.utills.Informations.WRONG_ATTENDANCE;

class Input {

    String getDateString() {
        Scanner sc = new Scanner(System.in);
        System.out.println(ENTER_DATE);
        return sc.nextLine();
    }

    int getParticipantID() {
        Scanner scanner = new Scanner(System.in);
        System.out.println(ENTER_ID);
        return scanner.nextInt();
    }

    String getSubject() {
        Scanner scanner = new Scanner(System.in);
        System.out.println(ENTER_SUBJECT);
        return scanner.next();
    }

    String getStatus(){
        Scanner scanner = new Scanner(System.in);
        System.out.println(SEPARATOR_3 + CHOOSE_STATUS + STATUS_ONE + STATUS_TWO + SEPARATOR_3);
        return scanner.nextLine();
    }

    String getAttendance(){
        Scanner sc = new Scanner(System.in);
        System.out.println(CHANGE_ATTENDANCE_POSITION);
        return sc.nextLine();
    }

    String getYesNoAnswer() {
        Scanner sc = new Scanner(System.in);
        System.out.println(CHANGE_ATTENDANCE);
        System.out.println(OPTION_ONE_TO_CHOOSE);
        System.out.println(OPTION_TWO_TO_CHOOSE);
        return sc.next().toUpperCase();
    }


    String getName() {
        Scanner scanner = new Scanner(System.in);
        System.out.println(ENTER_NAME);
        return scanner.next();
    }

    String getSurname() {
        Scanner scanner = new Scanner(System.in);
        System.out.println(ENTER_SURNAME);
        return scanner.next();
    }

    String getDateOfBirth() {
        Scanner scanner = new Scanner(System.in);
        System.out.println(ENTER_DOB);
        return scanner.next();
    }

    String getTelephoneNumber() {
        Scanner scanner = new Scanner(System.in);
        System.out.println(ENTER_TEL_NUM);
        return scanner.next();
    }

    String getLogin() {
        Scanner scanner = new Scanner(System.in);
        System.out.println(SET_LOGIN);
        return scanner.next();
    }

    String getPassword() {
        Scanner scanner = new Scanner(System.in);
        System.out.println(SET_PASSWORD);
        return scanner.next();
    }
}