package pl.tt.psc.kielce.attendanceRegister.utills;

import pl.tt.psc.kielce.attendanceRegister.model.Attendance;
import pl.tt.psc.kielce.attendanceRegister.model.Participant;
import static pl.tt.psc.kielce.attendanceRegister.utills.DateFormatter.*;
import static pl.tt.psc.kielce.attendanceRegister.utills.Informations.*;
import java.util.*;

import static pl.tt.psc.kielce.attendanceRegister.utills.Util.*;

public class Actions {

    private Util util = new Util();
    private Input input = new Input();


    void addNewParticipant() {
         Set<Participant> participants = Singleton.getInstance().getBaseParticipantData();
        String name = input.getName();
        String surname = input.getSurname();
        String dateOfBirth = input.getDateOfBirth();
        String telephoneNumber = input.getTelephoneNumber();
        String status = util.getStatus();
        String login = input.getLogin();
        String password = input.getPassword();
        Participant participant = new Participant(name, surname, dateOfBirth, telephoneNumber,
                status, login, password);
        if (util.isParticipantExisting(participant, participants)) {
            System.out.println(PARTICIPANT_EXIST);
        } else {
            participants.add(participant);
            System.out.println(COMPLETED);
        }
    }

    void displayAbsencesByParticipantID(Participant participant) {
        Set<Attendance> attendances = Singleton.getInstance().getBaseAttendanceData();
        attendances.stream().filter(attendance -> attendance.getParticipantID() == participant.getParticipantID())
                .filter(attendance -> !attendance.isAttendance()).forEach(System.out::println);
    }

    void addNewAttendance() {
        Set<Participant> participants = Singleton.getInstance().getBaseParticipantData();
        Set<Attendance> attendances = Singleton.getInstance().getBaseAttendanceData();
        participants.forEach(System.out::println);
        int participantID = input.getParticipantID();
        String subject = input.getSubject();
        Date date = new Date(System.currentTimeMillis());
        Attendance attendance2 = util.getAttendanceBy(participantID, subject, date, attendances);
        if (Objects.nonNull(attendance2)) {
            System.out.println(RECORD_EXIST);
        } else {
            boolean attendance = util.getAttendance();
            Attendance attendance1 = new Attendance(date, subject, attendance, participantID);
            attendances.add(attendance1);
            if (util.isAttendanceToEdit()) {
                util.editAttendance(attendance1);
            }
        }
    }

    void checkDailyAttendance() {
        Set<Participant> participants = Singleton.getInstance().getBaseParticipantData();
        Set<Attendance> attendances = Singleton.getInstance().getBaseAttendanceData();
        Date date = new Date(System.currentTimeMillis());
        System.out.println(date);
        String subject = input.getSubject();
        for (Participant participant : participants) {
            System.out.println(participant.toString());
            boolean attendance = util.getAttendance();
            Attendance attendance1 = new Attendance(date, subject, attendance, participant.getParticipantID());
            attendances.add(attendance1);
        }
    }

    void editAttendance() {
        Set<Participant> participants = Singleton.getInstance().getBaseParticipantData();
        Set<Attendance> attendances = Singleton.getInstance().getBaseAttendanceData();
        participants.forEach(System.out::println);
        int participantID = input.getParticipantID();
        String date = input.getDateString();
        Date date1 = formatDate(date);
        String subject = input.getSubject();
        Attendance attendance = util.getAttendanceBy(participantID, subject, date1, attendances);
        if (Objects.nonNull(attendance)) {
            boolean attendance2 = util.getAttendance();
            attendance.setAttendance(attendance2);
            System.out.println(COMPLETED);
        } else {
            System.out.println(NO_RECORDS);
        }
    }

    void displayParticipantsAttendanceByDate() {
        Set<Participant> participants = Singleton.getInstance().getBaseParticipantData();
        Set<Attendance> attendances = Singleton.getInstance().getBaseAttendanceData();
        String dateString = input.getDateString();
        if (Objects.nonNull(formatDate(dateString))) {
            List<Attendance> attendanceList = util.getAttendanceBy(dateString, attendances);
            if (!attendanceList.isEmpty()) {
                Set<Attendance> attendanceSet = new HashSet<>(attendanceList);
                System.out.println(getParticipantAttendances(participants, attendanceSet));
            } else {
                System.out.println(NO_RECORDS);
            }
        }
    }

    void displayAllAbsentParticipantsByDate() {
        Set<Participant> participants = Singleton.getInstance().getBaseParticipantData();
        Set<Attendance> attendances = Singleton.getInstance().getBaseAttendanceData();
        String dateInput = input.getDateString();
        if (Objects.nonNull(formatDate(dateInput))) {
            List<Attendance> attendanceList = util.getAbsencesBy(dateInput, attendances);
            if (!attendanceList.isEmpty()) {
                Set<Attendance> attendanceSet = new HashSet<>(attendanceList);
                System.out.println(getParticipantAttendances(participants, attendanceSet));
            } else {
                System.out.println(NO_RECORDS);
            }
        }
    }

    void exit() {
        int status = 0;
        System.exit(status);
    }
}