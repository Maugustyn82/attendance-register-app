package pl.tt.psc.kielce.attendanceRegister.model;

import static pl.tt.psc.kielce.attendanceRegister.utills.DateFormatter.*;
import java.util.Date;

public class Attendance {

    private Date date;
    private String subject;
    private boolean attendance;
    private int participantID;


    public Attendance(Date date, String subject, boolean attendance, int participantID) {
        this.date = date;
        this.subject = subject;
        this.attendance = attendance;
        this.participantID = participantID;
    }

    public Date getDate() {
        return date;
    }

    public String getSubject() {
        return subject;
    }

    public boolean isAttendance() {
        return attendance;
    }

    public int getParticipantID() {
        return participantID;
    }

    public void setSubject(String subject) {
        this.subject = subject;
    }

    public void setAttendance(boolean attendance) {
        this.attendance = attendance;
    }

    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append("Attendance: " + "\n");
        sb.append("date = " + date + "| ");
        sb.append("subject = " + subject + "| ");
        sb.append("attendance = " + attendance+ "\n");
        return sb.toString();
    }

    public String toStringCSV() {
        StringBuilder sb = new StringBuilder();
        sb.append(formatDateToString(date)+",");
        sb.append(subject + ",");
        sb.append(attendance + ",");
        sb.append(participantID);
        return sb.toString();
    }

}