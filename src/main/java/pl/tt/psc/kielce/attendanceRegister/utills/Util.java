package pl.tt.psc.kielce.attendanceRegister.utills;

import pl.tt.psc.kielce.attendanceRegister.model.Attendance;
import pl.tt.psc.kielce.attendanceRegister.model.Participant;

import java.util.*;

import static pl.tt.psc.kielce.attendanceRegister.utills.DateFormatter.formatDate;
import static pl.tt.psc.kielce.attendanceRegister.utills.Informations.*;

public class Util {

    private Input input = new Input();



    boolean isParticipantExisting(Participant participant, Set<Participant> participants) {
        return participants.stream().anyMatch(participantSet -> participantSet.equals(participant));
    }

    List<Attendance> getAttendanceBy(String date, Set<Attendance> attendancesSet) {
        List<Attendance> filteredAttendances = new ArrayList<>();
        attendancesSet.stream().filter(attendance -> attendance.getDate().equals(formatDate(date))).forEach
                (filteredAttendances::add);
        return filteredAttendances;
    }

    Attendance getAttendanceBy(int id, String subject, Date date, Set<Attendance> attendances) {
        return attendances.stream()
                .filter(attendance -> attendance.getParticipantID() == id)
                .filter(attendance -> attendance.getSubject().equalsIgnoreCase(subject))
                .filter(attendance -> attendance.getDate().equals(date)).findAny().orElse(null);
    }

    List<Attendance> getAbsencesBy(String date, Set<Attendance> attendanceSet) {
        List<Attendance> filteredAttendance = new ArrayList<>();
        List<Attendance> attendances = getAttendanceBy(date, attendanceSet);
        attendances.stream().filter(attendance1 -> !attendance1.isAttendance()).forEach(filteredAttendance::add);
        return filteredAttendance;
    }

    void editAttendance(Attendance attendance) {
        String choose = input.getAttendance();
        switch (choose) {
            case "1":
                attendance.setSubject(input.getSubject());
                break;
            case "2":
                attendance.setAttendance(getAttendance());
                break;
        }
    }

    boolean isAttendanceToEdit() {
        String choose = input.getYesNoAnswer();
        return choose.equalsIgnoreCase("Y");
    }

    boolean getAttendance() {
        //TODO T: Ta metoda to dla mnie enigma.
        // Masz juz troche informacji na temat refactoringu. To sprobuj poprawic te metode w ten sposob aby byla ona dla mnie czytelna.

        //tą metode zrobilem do sprawdzania i ustawiania obecnosci, sprawdzajacy ma tylko 2 opcje dzieki czemu
        // omijamy mozliwosc literówki w słowach false i true. To co metoda zwraca jest takze uzywane do zapisu
        // obecnosci na Csv, dlatego wczesniej zwracala mi ona stringa i mialem pewnosc ze na csv idzie string false
        // albo true, czy z booleanem bedzie tak samo?
        Scanner scanner = new Scanner(System.in);
        System.out.println(SEPARATOR_3 + CHOOSE_ATTENDANCE + ATTENDANCE_ONE + ATTENDANCE_TWO + SEPARATOR_3);
        String choose = scanner.nextLine();
        switch (choose) {
            case "1":
                return false;
            case "2":
                return true;
            default:
                System.out.println(WRONG_ATTENDANCE);
                getAttendance();
        }
        return false;
    }


    String getStatus() {

        String choose = input.getStatus();
        switch (choose) {
            case "1":
                return TEACHER;
            case "2":
                return STUDENT;
            default:
                System.out.println(WRONG_STATUS);
                getStatus();
        }
        return null;
    }

    private static List<Attendance> getAttendanceBy(int participantID, Set<Attendance> attendances) {
        List<Attendance> filteredAttendaces = new ArrayList<>();
        attendances.stream().filter(attendance -> attendance.getParticipantID() == participantID).forEach(filteredAttendaces::add);
        return filteredAttendaces;
    }

    public static Map<Participant, List<Attendance>> getParticipantAttendances(Set<Participant> participants,
                                                                               Set<Attendance> attendances) {
        Map<Participant, List<Attendance>> participantListMap = new TreeMap<>();
        participants.forEach(participant -> attendances.stream().filter(attendance ->
                participant.getParticipantID() == attendance.getParticipantID()).forEach(n -> participantListMap
                .put(participant, getAttendanceBy(participant.getParticipantID(), attendances))));

        return participantListMap;
    }
}