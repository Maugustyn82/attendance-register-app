package pl.tt.psc.kielce.attendanceRegister.utills;

import pl.tt.psc.kielce.attendanceRegister.model.Participant;
import java.util.Scanner;
import java.util.Set;
import static pl.tt.psc.kielce.attendanceRegister.utills.CSV.saveAllToCSV;
import static pl.tt.psc.kielce.attendanceRegister.utills.Informations.*;
import static pl.tt.psc.kielce.attendanceRegister.utills.Informations.SEPARATOR_3;

public class Controller {

    private Actions actions = new Actions();

    public void execute() {
        try {
            Participant participant = logon();

            if (participant.getStatus().equalsIgnoreCase(TEACHER)) {
                actionTeacher();
            }
            if (participant.getStatus().equalsIgnoreCase(STUDENT)) {
                actionStudent(participant);
            }
        } catch (NullPointerException e) {
            System.out.println(WRONG_LOG_DATA);
        }
    }


    private Participant logon() {
        Set<Participant> participants = Singleton.getInstance().getBaseParticipantData();
        Scanner sc = new Scanner(System.in);
        System.out.println(ENTER_LOGIN);
        String login = sc.nextLine();
        System.out.println(ENTER_PASSWORD);
        String password = sc.nextLine();
        for (Participant participant : participants) {
            if (participant.getLogin().equals(login) && participant.getPassword().equals(password)) {
                System.out.println(participant.toString());
                return participant;
            }
        }
        return null;
    }

    private void actionStudent(Participant participant) {
        String choose;
        do {
            choose = displayStudentMenu();

            switch (choose) {
                case "1":
                    actions.displayAbsencesByParticipantID(participant);
                    break;
                case "2":
                    actions.exit();
            }
        } while (!choose.equalsIgnoreCase(String.valueOf(2)));
    }

    private void actionTeacher() {
        String choose;
        do {
            choose = displayTeacherMenu();
            switch (choose) {
                case "1":
                    actions.addNewParticipant();
                    break;
                case "2":
                    actions.addNewAttendance();
                    break;
                case "3":
                    actions.checkDailyAttendance();
                    break;
                case "4":
                    actions.editAttendance();
                    break;
                case "5":
                    actions.displayParticipantsAttendanceByDate();

                    break;
                case "6":
                    actions.displayAllAbsentParticipantsByDate();

                case "7":
                    saveAllToCSV();
                    break;
                case "8":
                    actions.exit();
            }
        } while (!choose.equalsIgnoreCase(String.valueOf(8)));
        saveAllToCSV();
    }


    private String displayStudentMenu() {

        Scanner sc = new Scanner(System.in);
        System.out.println(SEPARATOR_3);
        System.out.println(SELECT_OPTION);
        System.out.println(STUDENT_OPTION_ONE + STUDENT_OPTION_TWO);
        System.out.println(SEPARATOR_3);
        return sc.next();
    }

    private String displayTeacherMenu() {
        Scanner sc = new Scanner(System.in);
        System.out.println(SEPARATOR_3);
        System.out.println(SELECT_OPTION);
        System.out.println(TEACHER_OPTION_ONE + TEACHER_OPTION_TWO + TEACHER_OPTION_THREE + TEACHER_OPTION_FOUR +
                TEACHER_OPTION_FIVE + TEACHER_OPTION_SIX + TEACHER_OPTION_SEVEN + TEACHER_OPTION_EIGHT);
        System.out.println(SEPARATOR_3);
        return sc.next();
    }
}
