package pl.tt.psc.kielce.attendanceRegister.utills;

import pl.tt.psc.kielce.attendanceRegister.model.Attendance;
import pl.tt.psc.kielce.attendanceRegister.model.Participant;
import static pl.tt.psc.kielce.attendanceRegister.utills.DateFormatter.*;
import java.io.*;
import java.util.*;

import static pl.tt.psc.kielce.attendanceRegister.utills.Informations.*;

public class CSV {

    public static Set<Participant> getParticipantData() {
        Set<Participant> participants = new TreeSet<>();
        String line = SPACE;
        try (BufferedReader reader = new BufferedReader(new FileReader(PARTICIPANT_DATA_PATH))) {
            while ((line = reader.readLine()) != null) {
                String firstSplit[] = line.split(REGEX_COMMA);
                String name = firstSplit[0];
                String surname = firstSplit[1];
                String dateOfBirth = firstSplit[2];
                String telephoneNumber = firstSplit[3];
                String status = firstSplit[4];
                String login = firstSplit[5];
                String password = firstSplit[6];
                Participant participant = new Participant(name, surname, dateOfBirth, telephoneNumber, status, login, password);
                participants.add(participant);
            }

        } catch (IOException e) {
            e.printStackTrace();
        }
        return participants;
    }

    public static Set<Attendance> getAttendanceData() {
        Set<Attendance> attendances = new HashSet<>();
        String line = SPACE;
        try (BufferedReader reader = new BufferedReader(new FileReader(ATTENDANCE_DATA_PATH))) {
            while ((line = reader.readLine()) != null) {
                String firstSplit[] = line.split(REGEX_COMMA);
                String data = firstSplit[0];
                String subject = firstSplit[1];
                boolean attendance = Boolean.parseBoolean(firstSplit[2]);
                int id = Integer.parseInt(firstSplit[3]);
                Attendance attendance1 = new Attendance(formatDate(data), subject, attendance, id);
                attendances.add(attendance1);
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
        return attendances;
    }

    private static void saveParticipantToCSV(Set<Participant> participants) {
        try (BufferedWriter writer = new BufferedWriter(new FileWriter(PARTICIPANT_DATA_PATH))) {
            for (Participant participant : participants) {
                writer.write(participant.toStringCSV());
                writer.write(NEW_LINE);
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    private static void saveAttendanceToCSV(Set<Attendance> attendances) {
        try (BufferedWriter writer = new BufferedWriter(new FileWriter(ATTENDANCE_DATA_PATH))) {
            for (Attendance attendance : attendances) {
                writer.write(attendance.toStringCSV());
                writer.write(NEW_LINE);
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    static void saveAllToCSV() {
        Set<Participant> participants = Singleton.getInstance().getBaseParticipantData();
        Set<Attendance> attendances = Singleton.getInstance().getBaseAttendanceData();
        saveParticipantToCSV(participants);
        saveAttendanceToCSV(attendances);
    }
}