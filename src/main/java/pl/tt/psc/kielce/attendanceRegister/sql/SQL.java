package pl.tt.psc.kielce.attendanceRegister.sql;

import java.sql.*;

public class SQL {
    public static void main(String[] args) {
        try {
            Class.forName("org.hsqldb.jdbcDriver");
            Connection connection = DriverManager.getConnection
                    ("jdbc:hsqldb:file:data/person.db", "sa", "");

            Statement statement = connection.createStatement();
            ResultSet resultSet = statement.executeQuery("SELECT * FROM \"PUBLIC\".\"PERSON\"");
            while (resultSet.next()) {
                System.out.println(resultSet.getString("name"));

            }

            statement.execute("SHUTDOWN");
            statement.close();
            connection.close();

        } catch (ClassNotFoundException | SQLException e) {
            e.printStackTrace();
        }

    }
}
