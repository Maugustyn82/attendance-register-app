package pl.tt.psc.kielce.attendanceRegister.utills;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import static pl.tt.psc.kielce.attendanceRegister.utills.Informations.*;

public class DateFormatter {

    static Date formatDate(String date) {
        SimpleDateFormat simpleDateFormat = new SimpleDateFormat(DATE_FORMAT);
        try {
            return simpleDateFormat.parse(date);

        } catch (ParseException e) {
            System.out.println(WRONG_DATE_FORMAT);
        }
        return null;
    }

    public static String formatDateToString(Date date) {
        SimpleDateFormat simpleDateFormat = new SimpleDateFormat(DATE_FORMAT);
        return simpleDateFormat.format(date);
    }
}